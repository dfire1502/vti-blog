import React from "react";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "./index.css";
import AdminPage from "./page/AdminPage/adminPage";
import Article from "./page/AdminPage/components/article/article";
import Statistical from "./page/AdminPage/components/statistical/statistical";
import LoginPage from "./page/LoginPage/loginPage";
import BlogPage from "./page/BlogPage/blogPage";
import BlogList from "./page/BlogPage/BlogList/blogList";
import BlogPosts from "./page/BlogPage/BlogPosts/blogPosts";
import LikePage from "./page/LikePage/likePage";
import GithubPage from "./page/GitgubPage/githubPage";
import AddArticle from "./page/AdminPage/components/article/products/addArticle/addArticle";

const router = createBrowserRouter([
  {
    path: "/admin",
    element: <AdminPage></AdminPage>,
    children: [
      {
        path: "article",
        element: <Article></Article>
      },
      {
        path: "statistical",
        element: <Statistical></Statistical>
      },
    ]
  },
  {
    path: "/like",
    element: <LikePage></LikePage>
  },
  {
    path: "/github",
    element: <GithubPage></GithubPage>
  },
  {
    path: "/login",
    element: <LoginPage></LoginPage>
  },
  {
    path: "/",
    element: <BlogPage></BlogPage>,
    children: [
      {
        path: "/",
        element: <BlogList></BlogList>
      },
      {
        path: "/post/:id",
        element: <BlogPosts></BlogPosts>
      }
    ]
  },
  {
    path: "/add",
    element: <AddArticle></AddArticle>
  },
]);
function App() {
  return (
    <>
      <RouterProvider router={router}/>
    </>
  );
}

export default App;
