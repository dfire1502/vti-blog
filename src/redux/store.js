import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk"
import Reducer from "../page/BlogPage/BlogPosts/redux/reducer";

const store = createStore(combineReducers({
    ids: Reducer
}), applyMiddleware(thunk))

export default store