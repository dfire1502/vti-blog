import React, { useEffect, useState } from 'react'
import ArticleComment from '../../../../common/api/articleComment'
import { useNavigate } from 'react-router-dom'
import { Button } from '@mui/material';
import './blogComment.css'
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import List from '@mui/material/List';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';

function BlogComment({article}) {
    const [comment, setComment] = useState('')
    const [cmts, setCmts] = useState ([])
    const [reload, setReload] = useState(false)
    const navigate = useNavigate()

    useEffect(()=>{
        const articleId = article.id
        ArticleComment.getComment(0,6,articleId)
        .then(response=>{
            const data = response.data
            
            const cmts=data.content
            setCmts(cmts)  
        })
        .catch(error=>{
            console.log(error);
        })
    },[reload])

    const handleCommentClicked = (e) => {
        e.preventDefault()
        ArticleComment.postComment(article.id,comment)
        .then(response => {
            setReload(!reload)
            setComment('')
        })
        .catch(error => {
            if(window.confirm('Đăng nhập để bình luận')){
                navigate('/login')
            }
        })
    }

    const handleCmtInput = (e) =>{
        setComment(e.target.value)
    }

  return (
    <>
        <Paper square sx={{ pb: '50px' }}>
            <Typography variant="h4" gutterBottom component="div" sx={{ p: 2, pb: 0 }}>
            Bình luận
            </Typography>
            <List sx={{ mb: 2 }}>
                <div className='blog-cmt'>
                    {cmts.map((cmt) => {
                        return(
                            <div key={cmt.id}>
                                <div className='avt-name'>
                                    <Avatar alt="Avatar" src={cmt.commentBy.avatarUrl} />
                                    <b className='cmt-name'>{cmt.commentBy.fullName}</b>
                                </div>
                                <div>{cmt.content}</div>
                            </div>
                        )
                    })}
                </div>
            </List>
        </Paper>
        <TextField
            id="outlined-multiline-static"
            label="Nhập bình luận vào đây"
            multiline
            rows={4}
            onChange={handleCmtInput}
            value={comment}
        />
        <Button className='cmt-button' variant="contained" onClick={handleCommentClicked}>Bình luận</Button>     
    </>
  )
}

export default BlogComment