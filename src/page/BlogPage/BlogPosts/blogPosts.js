import React, { useEffect, useState } from 'react'
import ArticleService from '../../../common/api/articleService'
import { useParams } from 'react-router-dom';
import BlogComment from './BlogComment/blogComment';
import Avatar from '@mui/material/Avatar';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import './blogPosts.css'
import Checkbox from '@mui/material/Checkbox';
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import Favorite from '@mui/icons-material/Favorite';
import Box from '@mui/material/Box';
import { useDispatch, useSelector } from 'react-redux';
import { addFavoriteAction, removeFavoriteAction } from './redux/action';


function BlogPosts() {
    const [article, setArticle] = useState([]);
    const [author, setAuthor] = useState([]);

    const param = useParams()
    const dispatch = useDispatch();
    const ids = useSelector(state => state.ids.ids)
    const [favorite, setFavorite] = useState(() => {
        return ids.includes(param.id)
    })

    const fetchArticleId = (id) => {
        ArticleService.getArticleId(id)
        .then(response => {
            const article = response.data;
            const author = article.author;
            setArticle(article)
            setAuthor(author)
            console.log(response);
        })
        .catch(error => {
            alert('Lỗi rồi!')
        });
        console.log('thanh cong');
    }
    useEffect(() => {
        fetchArticleId(param.id)
    }, [])

    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

    const handleFavoriteButtonClicked = () => {
        if(!favorite) {
            dispatch(addFavoriteAction(param.id))
        }
        else {
            dispatch(removeFavoriteAction(param.id))
        }
        setFavorite(!favorite)
    }
  return (
    <>
        <Box 
            display="grid" 
            marginLeft='58px' 
            gridTemplateColumns="repeat(1, 1fr)" 
            gap={1}>
            <img className='blog-img' src={article.coverImageUrl}/>
            <h1 className='blog-posts'>{article.title}</h1>
            <div className='blog-posts'>
                <Avatar alt="Avatar" src={author.avatarUrl} />
                <b className='blog-date'>{author.fullName}</b>
                <div className='blog-date'>{article.createdDate}</div>
                <div className='blog-date'><AutoStoriesIcon/> {article.estDuration}Phút đọc</div>
            </div>
            <p dangerouslySetInnerHTML={{__html: article.content }}></p>
            
            <h3 className='blog-posts'>Yêu thích</h3>
            <Checkbox {...label} icon={<FavoriteBorder />} checkedIcon={<Favorite />} isFavorite = {favorite} onClick={handleFavoriteButtonClicked} />
        
        
        <BlogComment
            key={article.id}
            article={article}
        />
        </Box>
       
    </>
  )
}

export default BlogPosts