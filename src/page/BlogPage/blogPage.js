import React from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../../common/components/Header/header'

const BlogPage = () => {
  return (
    <div>
        <Header/>
        <Outlet></Outlet>
    </div>
  )
}

export default BlogPage