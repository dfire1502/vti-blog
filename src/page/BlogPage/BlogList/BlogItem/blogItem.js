import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import './blogItem.css'
import { Link } from 'react-router-dom';

function BlogItem({article}) {

  return (
    <>
      <Card className='card-box' sx={{ maxWidth: 360 }}>
      <h1><Link to={"/post" + `/${article.id}`} title={article.title} className='blog-title'>{article.title}</Link></h1>
      <CardMedia
        component="img"
        height="194"
        image={article.coverImageUrl}
        alt="Paella dish"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        <p dangerouslySetInnerHTML={{__html: article.content }}></p>
        </Typography>
      </CardContent>
    </Card>
    </>
  )
}

export default BlogItem