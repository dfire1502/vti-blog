import React from 'react'
import ArticleService from '../../../common/api/articleService'
import { useState } from 'react'
import { useEffect } from 'react';
import BlogItem from './BlogItem/blogItem';
import Paging from '../../../common/components/Pagination/pagination';
import './blogList.css'
import Box from '@mui/material/Box';

function BlogList() {
  const [articles, setArticles] = useState([]);
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);

  const fetchArticles = (page, size) => {
    ArticleService.getArticles(page, size)
    .then(response => {
      const data = response.data;
      const articles = data.content;
      const totalPage = data.totalPages;
      setArticles(articles)
      setTotalPage(totalPage)
      console.log(response);
      if (totalPage <= page){
        setPage(totalPage-1)
      }
    })
    .catch(error => {
      console.log(error);
    })
  }
  useEffect(() => {
    fetchArticles(page, 4)
  },[page])

 
  return (
    <>
      <Box 
        display="grid" 
        marginLeft='58px' 
        gridTemplateColumns="repeat(4, 1fr)" 
        gap={1}>
      
        {articles.map((article) => {
          return(
            <BlogItem
            key={article.id}
            article={article}/>
          )
        })}
        </Box>
      
        <div className='blog-paging'>
        <Paging 
          onPageClick={(clickedpage)=>{setPage(clickedpage-1)}} 
          page={page + 1} 
          totalPage={totalPage} />
          </div>
    </>
  )
}

export default BlogList