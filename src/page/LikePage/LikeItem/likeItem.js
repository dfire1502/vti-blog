import React from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { addFavoriteAction, removeFavoriteAction } from '../../BlogPage/BlogPosts/redux/action'
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import Favorite from '@mui/icons-material/Favorite';
import CardActions from '@mui/material/CardActions';
import './likeItem.css'

const LikeItem = ({article}) => {
    const dispatch = useDispatch()
    const ids = useSelector(state => state.ids.ids)
    const [favorite, setFavorite] = useState(() => {
        return ids.includes(article.id)
    })
    const navigate = useNavigate()
    const handleLinkToBlogPost = () => {
        navigate('/post' + `/${article.id}`)
    }
    const handleFavoriteButtonClicked = () => {
        if(!favorite) {
            dispatch(addFavoriteAction(article.id))
        }
        else {
            dispatch(removeFavoriteAction(article.id))
        }
        setFavorite(!favorite)
    }

    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    return (
        <>
            <Card sx={{ maxWidth: 360 }}>
                <h1><Link to={"/post" + `/${article.id}`} title={article.title} className='blog-title'>{article.title}</Link></h1>
                <CardMedia
                    component="img"
                    height="194"
                    image={article.coverImageUrl}
                    alt="Paella dish"
                />
                <CardActions className='action-card'>
                    <Button size="small"  onClick={handleLinkToBlogPost}>Quay lại</Button>
                    <Checkbox {...label} icon={<FavoriteBorder />} checkedIcon={<Favorite />} isFavorite = {favorite} onClick={handleFavoriteButtonClicked} />
                </CardActions>
            </Card>
        </>
    )
}

export default LikeItem