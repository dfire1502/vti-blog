import React from 'react'
import { useState } from 'react'
import { useSelector } from 'react-redux';
import ArticleService from '../../common/api/articleService';
import { useEffect } from 'react';
import Header from '../../common/components/Header/header';
import Box from '@mui/material/Box';
import LikeItem from './LikeItem/likeItem';
import Paging from '../../common/components/Pagination/pagination';
import './likePage.css'

function LikePage() {
  const [articles, setArticles ] = useState([]);
  const [page, setPage] = useState(0)
  const [totalPage, setTotalPage] = useState(0)
  const ids = useSelector(state => state.ids.ids)

  const fetchArticles = (page,size) => {
    ArticleService.getArticles(page, size)
    .then(response => {
      const data = response.data;
      const articles = data.content;
      const totalPage = data.totalPages;
      setArticles(articles);
      setTotalPage(totalPage);
      if (totalPage <= page) {
        setPage(totalPage - 1)
      }
    })
    .catch(error => {
      console.log(error);
    })
  }
  useEffect(() => {
    fetchArticles(page, 100)
  }, [page])

  const favoriteArticle = () => {
    if(!articles) {
      return []
    }
    else {
      return articles.filter(article => ids.includes(article.id))
    }
  }
  return (
    <>
      <Header/>
      <h1 className='like-title'>Bài viết yêu thích</h1>
      <Box 
        display="grid" 
        marginLeft='58px' 
        gridTemplateColumns="repeat(4, 1fr)" 
        gap={1}>
        {favoriteArticle().map((article) => {
          return (
            <LikeItem
              key={article.id}
              article={article}
            />
          )
        })}
      </Box>
      <div className='like-title'>
        <Paging
          onPageClick={(clickedpage)=>{setPage(clickedpage-1)}} 
          page={page + 1} 
          totalPage={totalPage} />
      </div>
    </>
  )
}

export default LikePage