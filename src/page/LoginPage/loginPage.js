import React, { useEffect } from 'react'
import LoginForm from './components/loginForm'

function LoginPage() {
  return (
    <div>
      <LoginForm/>
    </div>
  )
}

export default LoginPage