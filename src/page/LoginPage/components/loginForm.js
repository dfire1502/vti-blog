import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import authService from '../../../common/api/authService';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import Input from '@mui/material/Input';
function LoginForm(onSubmit) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const navigate = useNavigate()
    useEffect(()=>{
        const token = localStorage.getItem('token')
        if(token){
        navigate('/')
        }
    },[])


    const handleLoginClicked = (e) => {
        e.preventDefault();
        authService.login(username, password)
        .then(response => {
            const data = response.data;
            localStorage.setItem('token', data.token)
            navigate('/')
        })
        .catch(error => {
            alert('Nhập lại tài khoản hoặc mật khẩu')
        })
    }

    const handleInputUsername = (e) => {
        setUsername(e.target.value)
    }

    const handleInputPassword = (e) => {
        setPassword(e.target.value)
    }
  return (
    <>
        <Box sx={{ '& > :not(style)': { m: 1 }, display: 'flex', justifyContent: 'center', marginTop: '200px'}}>
            <FormControl variant="standard">
                <h1>Đăng nhập</h1>
                <div class="form-outline mb-4">
                    <Input 
                        type="text" 
                        id="form2Example1" 
                        class="form-control" 
                        placeholder='Tên đăng nhập'
                        onChange={handleInputUsername}
                        value={username} />
                    </div>

                    <div class="form-outline mb-4">
                    <Input 
                        type="password" 
                        id="form2Example2" 
                        class="form-control" 
                        placeholder='Mật khẩu'
                        onChange={handleInputPassword}
                        value={password} />
                </div>
                <button 
                type="button" 
                class="btn btn-primary btn-block mb-4"
                onClick={handleLoginClicked}>Đăng nhập</button>
            </FormControl>
        </Box>
    </>
  )
}

export default LoginForm