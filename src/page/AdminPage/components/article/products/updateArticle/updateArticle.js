import React, { useRef, useState } from 'react'
import Button from '@mui/material/Button';
import fileService from '../../../../../../common/api/articleFile';
import { Editor } from '@tinymce/tinymce-react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
const UpdateArticle = ({article, onUpdateArticleClicked}) => {
    const [titleInput, setTitleInput] = useState(article.title);
    const [fileInput, setFileInput] = useState('');
    const [fileUrlInput, setFileUrlInput] = useState('');
    const [bodyInput, setBodyInput] = useState('');
    const editorRef = useRef()
    const handleTitleInput = (e) => {
        setTitleInput(e.target.value)
    }
    const handleFileInput = (e) => {
        setFileInput(e.target.files[0])
    }
    const handleUploadFile = () => {
        const formData = new FormData()
        formData.append('file', fileInput)
        fileService.ArticleFile(formData)
        .then(response => {
            const data = response.data
            const fileDownloadUrl = data.fileDownloadUri
            setFileUrlInput(fileDownloadUrl)
        })
        .catch(error => {
            alert('Lỗi rồi!')
        })
    }

    const handleBody = (e) => {
        setBodyInput(editorRef.current.getContent())
    }

    const articleInput = {title: titleInput, content: bodyInput, coverImageUrl: fileUrlInput}
    const handleAddButtonClicked = () =>{
      onUpdateArticleClicked(articleInput, article.id)
  }
  return (
    <Box
        sx={{
          width: 1330,
          maxWidth: '100%',
          margin: '50px'
          
        }}
      >
        <h1>Thêm bài viết mới</h1>
        <TextField fullWidth label="Tiêu đề bài viết" id="fullWidth"
            onChange={handleTitleInput}
            value={titleInput}/>

        <h3 className='add-margin'>Ảnh bìa</h3>
        <TextField fullWidth label="Thêm ảnh bìa" id="fullWidth"
            onChange={handleFileInput} /><br/><br/>

        <Button 
            variant="contained"
            onClick={handleUploadFile}>
            Upload
        </Button>

        <h4 className='add-margin'>Nội dung</h4>
        <Editor
            textareaName='content'
            onEditorChange={handleBody}
            onInit={(e,editor)=> editorRef.current = editor}
        /><br/>
        
        <Button variant="contained"
            onClick={handleAddButtonClicked}>
            Lưu bài viết
        </Button>   
        </Box>
  )
}

export default UpdateArticle