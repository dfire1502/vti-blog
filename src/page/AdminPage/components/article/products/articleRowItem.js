import Button from '@mui/material/Button';
import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import UpdateArticle from './updateArticle/updateArticle';
import ArticleAdmin from '../../../../../common/api/articleAdmin';
import DeleteIcon from '@mui/icons-material/Delete';
import { styled } from '@mui/material/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import EditCalendarIcon from '@mui/icons-material/EditCalendar';
import { purple } from '@mui/material/colors';

const ArticleRowItem = ({article, onDeleteArticle, onUpdateArticle}) => {
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 1500,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  }
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const [open, setOpen] = React.useState(false);
const [articles, setArticles] = useState([])
const [author, setAuthor] = useState([])
const handleOpen = () => setOpen(true);
const handleClose = () => setOpen(false);

const handleDeleteArticle = () => {
  if(onDeleteArticle){
    onDeleteArticle(article.id)
  }
}

const fetchArticlesId = (id) =>{
  ArticleAdmin.getArticleId(id)
  .then(response =>{
      const articles = response.data;
      setArticles(articles);
      setAuthor(author)
  })
  .catch(error =>{
      console.log(error);
  })
};
useEffect(()=>{
      fetchArticlesId(article.id)
},[])

const handleUpdateButtonClicked = (article, articleId) => {
  if(onUpdateArticle){
    onUpdateArticle(article, articleId)
  };
}

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: purple[500],
  '&:hover': {
    backgroundColor: purple[700],
  },
}));
  return (
    <>
      <StyledTableRow>
        <StyledTableCell align="left">{article.title}</StyledTableCell>
        <StyledTableCell align="left">{article.author.fullName}</StyledTableCell>
        <StyledTableCell align="left">{article.createdDate}</StyledTableCell>
        <StyledTableCell align="left">
          <Button variant="contained"
            startIcon={<EditCalendarIcon />}
            onClick={handleOpen}>
            Sửa
          </Button>
        </StyledTableCell>
        <StyledTableCell align="left">
          <ColorButton variant="contained"
            startIcon={<DeleteIcon />}
            onClick={handleDeleteArticle}>
            Xóa
          </ColorButton>
        </StyledTableCell>
        </StyledTableRow>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <UpdateArticle
              key={articles.id}
              article={articles}
              onUpdateArticleClicked={handleUpdateButtonClicked}
            />
          </Typography>
        </Box>
      </Modal>
    </>
  )
}

export default ArticleRowItem