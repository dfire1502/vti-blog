import React, { useRef, useState } from 'react'
import Button from '@mui/material/Button';
import { Editor } from '@tinymce/tinymce-react';
import './addArticle.css'
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import fileService from '../../../../../../common/api/articleFile';
import ArticleAdmin from '../../../../../../common/api/articleAdmin';
import { Input } from '@mui/material';

const AddArticle = () => {
    const [titleInput, setTitleInput] = useState('');
    const [fileInput, setFileInput] = useState('');
    const [fileUrlInput, setFileUrlInput] = useState('');
    const [bodyInput, setBodyInput] = useState('')
    const navigate = useNavigate()
    const editorRef = useRef()

    const handleTitleInput = (e) => {
        setTitleInput(e.target.value)
    }
    const handleFileInput = (e) => {
        setFileInput(e.target.files[0])
    }
    const handleUploadFile = () => {
        const formData = new FormData()
        formData.append('file', fileInput)
        fileService.ArticleFile(formData)
        .then(response => {
            const data = response.data
            const fileDownloadUrl = data.fileDownloadUri
            setFileUrlInput(fileDownloadUrl)
            console.log(fileUrlInput);
        })
        .catch(error => {
            alert('Lỗi rồi!')
        })
    }

    const handleBody = (e) => {
        setBodyInput(editorRef.current.getContent())
    }

    const articleInput = {title: titleInput, content: bodyInput, coverImageUrl: fileUrlInput}
    const handleCreateArticle = (article) =>{
        ArticleAdmin.createArticle(article)
        .then(response => {
            alert('Thêm thành công');
        })
        .catch(error => {
            alert('Thêm bài viết không thành công !!!');
        })
    }
    const handleAddButtonClicked = () =>{
        handleCreateArticle(articleInput)
        navigate('/')
    }
    const handleAddButtonOut = () => {
        navigate('/admin/article')
    }
    return (
        <Box
        sx={{
          width: 1800,
          maxWidth: '100%',
          margin: '50px'
          
        }}
      >
        <h1>Thêm bài viết mới</h1>
        <TextField fullWidth label="Tiêu đề bài viết" id="fullWidth"
            onChange={handleTitleInput}
            value={titleInput}/>

        <h3 className='add-margin'>Ảnh bìa</h3>
        <Input
            type='file'
            onChange={handleFileInput} /><br/><br/>
        <Button 
            variant="contained"
            onClick={handleUploadFile}>
            Upload
        </Button>

        <h4 className='add-margin'>Nội dung</h4>
        <Editor
            textareaName='content'
            onEditorChange={handleBody}
            onInit={(e,editor)=> editorRef.current = editor}
        /><br/>
        
        <Button variant="contained"
            onClick={handleAddButtonClicked}>
            Lưu bài viết
        </Button><br/><br/>
        <Button variant="contained" color="success"
            onClick={handleAddButtonOut}>
            Quay lại
        </Button>
        </Box>
  )
}

export default AddArticle