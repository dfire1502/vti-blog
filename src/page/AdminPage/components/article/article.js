import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import ArticleRowItem from './products/articleRowItem';
import Paging from '../../../../common/components/Pagination/pagination';
import ArticleAdmin from '../../../../common/api/articleAdmin';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

function Article() {
  const [article, setArticle] = useState([]);
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [inputTitle, setInputTitle] = useState('');
  const [inputDataTo, setInputDataTo] = useState('');
  const [inputDataFrom, setInputDataFrom] = useState('');
  const [reload, setReload] = useState(false);
  const navigate = useNavigate()

  const body = {
    title: inputTitle,
    createdDateFrom: inputDataFrom,
    createdDateTo: inputDataTo
  }

  const fetchArticle = (page, size, body) => {
    ArticleAdmin.getArticles(page, size, body)
    .then(response => {
      const data = response.data;
      const article = data.content;
      const totalPage = data.totalPages;
      
      setArticle(article);
      setTotalPage(totalPage);
      if(totalPage <= page){
        setPage(totalPage - 1)
      }
    })
    .catch(error => {
      console.log(error);
    })
  }
  useEffect(() => {
    fetchArticle(page, 8, body)
  }, [page, reload])

  const handleDeleteArticle = (articleId) => {
    if(window.confirm('Bạn có muốn xóa bài viết này?')){
      ArticleAdmin.deleteArticle(articleId)
      .then(response => {
        alert('Xóa thành công!');
        setReload(!reload)
      })
      .catch(error => {
        alert('Đã xảy ra lỗi!')
      })
    }
  }

  const handleUpdateArticle = (article, articleId) =>{
    ArticleAdmin.updateArticle(article, articleId)
    .then(response => {
        alert('Sửa bài viết thành công');
        setReload(!reload);
    })
    .catch(error => {
        alert('Không sửa được bài của người khác đâu cưng ^^')
    })
    console.log(article);
    console.log(articleId);
  }

  const handleInputTitle = (e) => {
    setInputTitle(e.target.value)
  }
  const handleInputDateTo = (e) => {
    setInputDataTo(e.target.value)
  }
  const handleInputDateFrom = (e) => {
    setInputDataFrom(e.target.value)
  }
  const handleButtonSearch = () => {
    fetchArticle(page, 10, body)
  }
  const handleButtonAdd = () =>{
    navigate('/add')
    setReload(!reload)
  }

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    }
  }));
  
  return (
    <>
      <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 2, width: '1600px' },
      }}
      noValidate
      autoComplete="off"
      >
        <h3>Danh sách bài viết</h3>
        <Box
        component="form"
        sx={{
          '& > :not(style)': { m: 1, width: '25ch', height: '55px' },
        }}
        noValidate
        autoComplete="off">
        <TextField id="outlined-basic" label="Nhập vào tiêu đề" variant="outlined"
            aria-label="Recipient's username with two button addons"
            onChange={handleInputTitle}
            value={inputTitle}
          />
          <TextField id="outlined-basic" label="Ngày đăng bắt đầu từ" variant="outlined"
            aria-label="Recipient's username with two button addons"
            onChange={handleInputDateTo}
            value={inputDataTo}
          />
          <TextField id="outlined-basic" label="Ngày đăng đến ngày" variant="outlined"
            aria-label="Recipient's username with two button addons"
            onChange={handleInputDateFrom}
            value={inputDataFrom}
          />
          <Button variant="contained" onClick={handleButtonSearch}>Tìm kiếm</Button>
          <Button variant="contained" onClick={handleButtonAdd}>Thêm mới</Button>
        </Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Tên bài viết</StyledTableCell>
                  <StyledTableCell align="left">Tác giả</StyledTableCell>
                  <StyledTableCell align="left">Ngày đăng</StyledTableCell>
                  <StyledTableCell align="left"></StyledTableCell>
                  <StyledTableCell align="left"></StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {article.map((article) => (
                  <ArticleRowItem
                    key={article.id}
                    article={article}
                    onDeleteArticle={handleDeleteArticle}
                    onUpdateArticle={handleUpdateArticle}
                  />
                ))}
              </TableBody>
            </Table>
          </TableContainer>

        <Paging
          onPageClick={(clickedpage)=>{setPage(clickedpage-1)}} 
          page={page + 1} 
          totalPage={totalPage}/>
      </Box>
    </>
  );
}

export default Article;