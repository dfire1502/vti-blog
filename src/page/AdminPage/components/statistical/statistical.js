import React, { useEffect, useState } from 'react'
import ArticleAdmin from '../../../../common/api/articleAdmin'
import {
  Chart as ChartJS,
  BarElement,
  CategoryScale,
  LinearScale,
  Tooltip,
  Legend
} from "chart.js";
import { Bar } from 'react-chartjs-2'
import './statistical.css'

ChartJS.register (
  BarElement,
  CategoryScale,
  LinearScale,
  Tooltip,
  Legend
)

const Statistical = () => {
  const [chartData, setChartData] = useState([])
  const [labels, setLabels] = useState([])

  useEffect(() => {
    const fetchStatistical = () => {
      ArticleAdmin.getStatistical()
      .then(response => {
        const chartData = response.data.data
        const labels = response.data.labels
        setChartData(chartData)
        setLabels(labels)
      })
      .catch(error => {
        console.log(error);
      })
    }
    fetchStatistical()
  }, [])

  const data = {
    labels: labels,
    datasets: [
      {
        label: 'Số lượt xem',
        data: chartData,
        backgroundColor: '#1976D2',
        borderColor: 'black',
        borderWidth: 1,
      }
    ]
  }

  const options = {}
  return (
    <>
      <h1>Thống kê số lượt xem trong tuần</h1>
      <div className='statistical-bar'>
        <Bar
          data = {data}
          options = {options}
        ></Bar>
      </div>
    </>
  )}
export default Statistical