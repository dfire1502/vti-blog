import * as React from 'react';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { useNavigate } from 'react-router-dom';
import Container from '@mui/material/Container';
import AdbIcon from '@mui/icons-material/Adb';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import { AppBar } from '@mui/material';

function Header() {
  const navigate = useNavigate()
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    navigate("/")
  }

  const handleGoToArticlePage = () => {
    navigate("/")
  }
  const handleGoToLike = () => {
    navigate("/like")
  }
  const handleGoToGithub =() => {
    navigate("/github")
  }
  const handleGoToAdmin = () => {
    navigate("admin")
  }
  const handleLogin = () => {
    navigate("login")
  }

  const isLoggedIn = localStorage.getItem("token");
  return (
    <>
      <AppBar position="static">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component="a"
              href="/"
              sx={{
                mr: 2,
                display: { xs: 'none', md: 'flex' },
                fontFamily: 'monospace',
                fontWeight: 700,
                letterSpacing: '.3rem',
                color: 'inherit',
                textDecoration: 'none',
              }}>
                VTI BLOG
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
            </Menu>
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            
              <Button
                
                onClick={handleGoToArticlePage}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                Trang chủ
              </Button>
              <Button
                onClick={handleGoToLike}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                Yêu thích
              </Button>
              <Button
                onClick={handleGoToGithub}
                sx={{ my: 2, color: 'white', display: 'block' }}
              >
                Github
              </Button>
          </Box>
            { !isLoggedIn &&
              <Button
              onClick={handleLogin}
              sx={{ my: 2, color: 'white', display: 'block' }}
            >
              Đăng nhập
            </Button>
            }
      
            {
              isLoggedIn &&
                <>
                  <Button
                    onClick={handleGoToAdmin}
                    sx={{ my: 2, color: 'white', display: 'block' }}
                  >
                    Admin
                  </Button>
                  <Button
                    onClick={handleLogout}
                    sx={{ my: 2, color: 'white', display: 'block' }}
                  >
                    Đăng xuất
                  </Button>
                  <Avatar alt="Avatar" src="https://ss-images.saostar.vn/pc/1594552169806/eujb4tnwsaukzif.jpg" />
                </>
            }
        </Toolbar>
      </Container>
    </AppBar>
    </>
  );
}

export default Header;