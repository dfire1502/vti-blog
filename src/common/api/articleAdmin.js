import axiosClient from "./axiosClient";

const BASE_PATH = "/api/v1/admin"

const getHeaderConfig = () => {
    return {
        headers: {
            token: localStorage.getItem('token')
        }
    }
}
const getArticles = (page, size) => {
    return axiosClient.post(BASE_PATH + '/articles' + '/filter' + `?page=${page}&size=${size}`,{}, getHeaderConfig());
}

const deleteArticle = (articleId) => {
    return axiosClient.delete(BASE_PATH +'/articles' + `/${articleId}`, getHeaderConfig());
}

const createArticle = (article) => {
    return axiosClient.post(BASE_PATH + '/articles', article, getHeaderConfig());
}

const updateArticle = (article, articleId) => {
    return axiosClient.put(BASE_PATH +'/articles' + `/${articleId}`, article, getHeaderConfig());
}

const getStatistical = () =>{
    return axiosClient.get(BASE_PATH + '/statistic/by-week', getHeaderConfig())
}

const getArticleId = (articleId) =>{
    return axiosClient.get(BASE_PATH + '/articles' + `/${articleId}`, getHeaderConfig())
}

const ArticleAdmin = {
    getArticles,
    deleteArticle,
    createArticle,
    updateArticle,
    getStatistical,
    getArticleId
};

export default ArticleAdmin;