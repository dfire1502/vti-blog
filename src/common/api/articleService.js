import axiosClient from "./axiosClient";

const BASE_PATH = '/api/v1/articles';

const getArticles = (page, size) => {
    return axiosClient.get(BASE_PATH + `?page=${page}&size=${size}`);
}

const getArticleId = (articleId) =>{
    return axiosClient.get(BASE_PATH + `/${articleId}`)
}

const ArticleService = {
    getArticles,
    getArticleId
}

export default ArticleService