import axiosClient from "./axiosClient";

const BASE_PATH = '/api/v1/files';

const ArticleFile = (file) =>{
    return axiosClient.post(BASE_PATH, file )
}

const fileService = {
    ArticleFile
}
export default fileService