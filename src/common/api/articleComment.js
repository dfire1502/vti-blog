import axiosClient from "./axiosClient";

const BASE_PATH = '/api/v1/comments'

const getHeaderConfig = () => {
    return {
        headers: {
            token: localStorage.getItem('token')
        }
    }
}

const postComment = (articleId, content) => {
    return axiosClient.post(BASE_PATH + `/${articleId}`, {content}, getHeaderConfig())
}
const getComment = (page,size,articleId) =>{
    return axiosClient.get(BASE_PATH + `/${articleId}` + `?page=${page}&size=${size}`)
}

const ArticleComment = {
    postComment,
    getComment
}
export default ArticleComment