import axiosClient from "./axiosClient";

const BASE_PATH = '/api/v1/auth';

const login = (username, password) => {
    return axiosClient.post(BASE_PATH + `/login`, {username, password});
}

const authService = {
    login
}

export default authService;